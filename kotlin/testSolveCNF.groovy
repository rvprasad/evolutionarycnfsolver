/*
 * Copyright (c) 2017, Venkatesh-Prasad Ranganath
 *
 * BSD 3-clause License
 *
 * Author: Venkatesh-Prasad Ranganath (rvprasad)
 */

@Grab(group='org.spockframework', module='spock-core', version='2.0-M3-groovy-3.0', transitive=false)

import spock.lang.Unroll

class TestModelCheck extends spock.lang.Specification {
    def executeCNFSolverWithArgs(args) {
        final check = (['kscript', 'solvecnf.kts'] + args).join(' ')
        final outStream = new ByteArrayOutputStream()
        final proc = check.execute()
        proc.consumeProcessOutputStream(new PrintStream(outStream))
        proc.waitFor()
        final sep = System.getProperty('line.separator')
        return outStream.toString().split(sep) as List
    }

    @Unroll
    def 'Correctly solves CNF #cnfFile'() {
        when: 'Satisfiable CNF is solved with solvecnf.kts'
        final args = ['-c', cnfFile, '-p', population as String, '-g', gens as String]
        final out = executeCNFSolverWithArgs(args)

        then: 'expected number of output lines are generated'
        out.size() == size

        and: 'matching number of seen and expected variables are reported'
        out[0] == "c Variables $vars seen $vars expected"

        and: 'matching number of seen and expected clauses are reported'
        out[1] == "c Clauses $clauses seen $clauses expected"

        and: 'gens are reported'
        out[2] ==~ /^c gen \d+$/

        and: 'satisfiable verdict are reported'
        out[3] == "s $verdict"

        and: 'model is consistent, if satisfiable'
        if (verdict == 'SATISFIABLE') {
            final model = out[4].tokenize()[1..-1]*.toInteger()
            model.every { !(-it in model) }
        }

        and: 'model satisfies the CNF, if satisfiable'
        if (verdict == 'SATISFIABLE') {
            final model = out[4].tokenize()[1..-1]*.toInteger()
            new File(cnfFile).newReader().eachLine { line ->
                line = line.trim()
                if (line ==~ /-?\d.*/) {
                    final tmp1 = line.tokenize().collect { it.toInteger() }.takeWhile { it != 0 }
                    tmp1.any { it in model }
                }
            }
        }

        where:
        cnfFile                               | gens | population || size | vars | clauses | verdict
        '../test-inputs/satisfiable-1.cnf'    | 10   | 10         || 5    | 7    | 11      | 'SATISFIABLE'
        '../test-inputs/satisfiable-2.cnf'    | 10   | 10         || 5    | 10   | 25      | 'SATISFIABLE'
        '../test-inputs/satisfiable-3.cnf'    | 50   | 750        || 5    | 25   | 100     | 'SATISFIABLE'
        '../test-inputs/satisfiable-4.cnf'    | 75   | 1500       || 5    | 50   | 200     | 'SATISFIABLE'
        '../test-inputs/unsatisfiable-1.cnf'  | 100  | 400        || 4    | 25   | 150     | 'UNSATISFIABLE'
        '../test-inputs/unsatisfiable-2.cnf'  | 100  | 400        || 4    | 4    | 20      | 'UNSATISFIABLE'
        '../test-inputs/unsatisfiable-3.cnf'  | 100  | 400        || 4    | 100  | 600     | 'UNSATISFIABLE'
        '../test-inputs/unsatisfiable-4.cnf'  | 1    | 1          || 4    | 0    | 1       | 'UNSATISFIABLE'
    }
}
