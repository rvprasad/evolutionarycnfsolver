/*
 * Copyright (c) 2019 Venkatesh-Prasad Ranganath
 *
 * BSD 3-clause License
 *
 * Author: Venkatesh-Prasad Ranganath (rvprasad)
 */

@file:CompilerOpts("-jvm-target 1.8")
@file:DependsOn("com.github.ajalt:clikt:2.8.0")
@file:DependsOn("org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.3.72")
//@file:KotlinOpts("-J-ea")

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.required
import com.github.ajalt.clikt.parameters.types.float
import com.github.ajalt.clikt.parameters.types.int
import java.io.File
import java.util.Optional
import java.util.Random
import java.util.concurrent.ThreadLocalRandom
import kotlin.math.abs
import kotlin.math.absoluteValue
import kotlin.streams.toList
import kotlin.text.Regex

private typealias Assignment = Set<Int>
private typealias Clause = List<Int>
private typealias Var = UInt

internal class CommandLine: CliktCommand(name = "kscript solvecnf.kts",
        printHelpOnEmptyArgs = true,
        help = "Evolutionary algorithm based CNF solver") {
    private val cnf by option("-c", "--cnf", help = "CNF file in DIMACS format " +
            "(http://www.satcompetition.org/2009/format-benchmarks2009.html)").required()
    private val maxGens by option("-g", "--maxGens", help = "Maximum number of generations.  (Default: " +
            "#vars * 2)").int()
    private val populationSize by option("-p", "--populationSize", help = "Population size.  (Default: " +
            "#clauses * 8)").int()
    private val mutationThreshold by option(help = "Mutation threshold.  (Default: 0.8)").float().default(0.8f)

    override fun run() {
        val (clauses, vars) = readClauses(cnf)
        val maxGens = maxGens?.absoluteValue ?: vars.size * 2
        val popSize = populationSize?.absoluteValue ?: clauses.size * 8
        val mutThres = mutationThreshold?.absoluteValue ?: 0.8f
        EvolutionaryCNFSolver.solve(clauses, vars, popSize, mutThres, maxGens).run {
            println("c gen $second")
            if (first.isEmpty()) {
                println("s UNSATISFIABLE")
            } else {
                println("s SATISFIABLE")
                println("v ${first.sortedBy(::abs).joinToString(" ")} 0")
            }
        }
    }

    private fun readClauses(fileName: String): Pair<Collection<Clause>, Set<Var>> {
        var expectedNumVars = 0
        var expectedNumClauses = 0
        val infoRegex = Regex("^p cnf \\d+ \\d+.*")
        val clauseRegex = Regex("^\\s*-?\\d.*")
        fun tokenize(line: String) = line.trim().split(' ').filter { it.isNotBlank() }

        val clauses = File(fileName).bufferedReader().lineSequence().map { line ->
            val tokens = tokenize(line)
            when {
                infoRegex.containsMatchIn(line) -> {
                    expectedNumVars = Integer.parseInt(tokens[2])
                    expectedNumClauses = Integer.parseInt(tokens[3])
                    null
                }
                clauseRegex.containsMatchIn(line) -> tokens.map { it.toInt() }.takeWhile { it != 0 }.toMutableList()
                else -> null
            }
        }.filterNotNull().toList()

        val seenVars = clauses.flatten().map { it.absoluteValue.toUInt() }.toSet()
        println("c Variables ${seenVars.size} seen $expectedNumVars expected")
        println("c Clauses ${clauses.size} seen $expectedNumClauses expected")

        return Pair(clauses, seenVars)
    }
}

private class EvolutionaryCNFSolver(val clauses: Collection<Clause>, val vars: Set<Var>, popSize: Int,
                                    val mutationThreshold: Float) {
    private var populationSize = popSize + (popSize % 2)
    var currGen = 0
        private set

    private val numClauses = clauses.size
    private val numVars = vars.size

    internal companion object {
        internal fun solve(givenClauses: Collection<Clause>, givenVars: Set<Var>, popSize: Int,
                mutationThreshold: Float, maxGens: Int): Pair<Assignment, Int> {
            val verdict = performBCP(givenClauses.map { it.distinct() })
            return when (verdict) {
                is Verdict.Conflicts -> Pair(emptySet(), 0)
                is Verdict.Satisfied -> Pair(verdict.assignment, 0)
                is Verdict.Unresolved -> {
                    val assignmentFromBCP = verdict.partialAssignment
                    val remainingVars = givenVars.filter { v ->
                        v.toInt().let { it !in assignmentFromBCP && -it !in assignmentFromBCP }
                    }.toSet()
                    EvolutionaryCNFSolver(verdict.clauses, remainingVars, popSize, mutationThreshold).let {
                        val assignment = it.solve(maxGens)
                        Pair(if (assignment.isEmpty()) emptySet() else assignment + assignmentFromBCP, it.currGen)
                    }
                }
            }
        }

        private fun simplifyClausesBasedOnValues(clauses: Collection<Clause>, assignment: Assignment) =
                clauses.filterNot { c -> c.any { it in assignment } }.map { c -> c.filterNot { -it in assignment } }

        internal sealed class Verdict {
            class Satisfied(val assignment: Assignment): Verdict()
            object Conflicts: Verdict()
            class Unresolved(val clauses: Collection<Clause>, val partialAssignment: Assignment): Verdict()
        }

        private tailrec fun performBCP(clauses: Collection<Clause>, assignment: Assignment = emptySet()): Verdict {
            val valsFromUnitClauses = assignment + clauses.filter { it.size == 1 }.map { it[0] }.toSet()
            val simplifiedClauses = simplifyClausesBasedOnValues(clauses, valsFromUnitClauses)
            val newAssignment = assignment + valsFromUnitClauses
            return when {
                newAssignment.any { -it in newAssignment } || simplifiedClauses.any { it.isEmpty() } ->
                    Verdict.Conflicts
                simplifiedClauses.isEmpty() -> Verdict.Satisfied(newAssignment)
                clauses.size != simplifiedClauses.size -> performBCP(simplifiedClauses, newAssignment)
                else -> Verdict.Unresolved(simplifiedClauses, newAssignment)
            }
        }
    }

    data class Chromosome(val chrom: List<Int>, val fitness: Int)

    private fun createChromosome(chrom: List<Int>) =
            Chromosome(chrom, clauses.count { c -> c.any(chrom::contains) })

    private val random: Random
        get() = ThreadLocalRandom.current()

    private fun initialize(): Collection<Chromosome> =
            vars.map(Var::toInt).run {
                parallelStream().flatMap { v ->
                    val chrom = map { if (it == v) it else -it }
                    listOf(createChromosome(chrom), createChromosome(chrom.map { -it })).stream()
                }.toList() + createChromosome(this.toList()) + createChromosome(map { -it })
            }.sortedByDescending { it.fitness }.take(populationSize)

    private fun recombine(population: Collection<Chromosome>): Collection<Chromosome> {
        tailrec fun helper(acc: MutableList<Chromosome>, popCopy: Collection<Chromosome>): Collection<Chromosome> {
            acc.addAll(popCopy.filter { random.nextInt(numClauses) + 1 <= it.fitness })
            return if (acc.size > populationSize) acc else helper(acc, popCopy)
        }

        fun betterThanAtLeastOne(chrom2: Chromosome, chrom3: Chromosome) =
                { chrom1: Chromosome -> chrom1.fitness >= chrom2.fitness || chrom1.fitness >= chrom3.fitness }

        fun combine(chrom1: Chromosome, chrom2: Chromosome, pos: Int) =
                (chrom1.chrom.slice(0 until pos) + chrom2.chrom.slice(pos until numVars)).let(::createChromosome)

        val parents = helper(ArrayList(), population.shuffled()).take(populationSize)
        val tmp1 = parents.sortedByDescending { it.fitness }
        return (0 until (populationSize / 2)).toList().parallelStream().flatMap {
            val i = it * 2
            val parent1 = tmp1[i]
            val parent2 = tmp1[i + 1]
            assert(numVars > 0) { numVars.toString() }
            val pos = random.nextInt(numVars)
            val comb12 = combine(parent1, parent2, pos)
            val comb21 = combine(parent2, parent1, pos)
            listOf(comb12, comb21).filter(betterThanAtLeastOne(parent1, parent2)).stream()
        }.distinct().toList()
    }

    private fun mutate(chromosomes: Collection<Chromosome>): Collection<Chromosome> {
        fun flipAllele(chrom: Chromosome, pos: Int) =
                chrom.chrom.map { if (it != pos) it else -it }.let(::createChromosome)

        return chromosomes.parallelStream().map { c ->
            val tmp1 = flipAllele(c, random.nextInt(numVars))
            if (tmp1.fitness >= c.fitness || random.nextFloat() > mutationThreshold) tmp1 else c
        }.distinct().toList()
    }

    private fun selectSurvivors(population: Collection<Chromosome>) =
            population.sortedByDescending { it.fitness }.take(populationSize).distinct()

    private fun checkSAT(pool: Collection<Chromosome>) = pool.find { it.fitness == numClauses }

    private fun solve(maxGens: Int): Assignment {
        currGen = 0
        var population = initialize()
        val tmp1 = checkSAT(population)
        if (tmp1 != null)
            return tmp1.chrom.toSet()

        return (1..maxGens).asSequence().mapNotNull { gen ->
            val recombinations = recombine(population)
            checkSAT(recombinations)?.run { return@mapNotNull chrom.toSet() }
            population = (population + mutate(recombinations)).run(::selectSurvivors)
            checkSAT(population)?.run{ return@mapNotNull chrom.toSet() }
            currGen = gen
            null
        }.firstOrNull() ?: emptySet()
    }
}

CommandLine().main(args)
