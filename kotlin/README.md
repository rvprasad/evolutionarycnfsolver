# EvolutionaryCNFSolver

Implementation of an evolutionary algorithm to solve non-trivial CNFs.  It provides the following information: # of seen/expected variables, # of seen/expected clauses, if the CNF is satisfiable, and the model for satisfiable CNFs.  

**Note:** This is an adaptation of the Groovy implementation that was retired.


## Requirements
- JDK 1.8+.  We use JDKs from the following sources.
    - [Oracle](http://www.oracle.com/technetwork/java/javase/%20downloads/index.html)
    - [Azul Systems](https://www.azul.com/products/zulu/)
- [kscript 2.8.0](https://github.com/holgerbrandl/kscript)
- [For Development] [Kotlin 1.3.41](https://kotlinlang.org/)
- [For Development] [Clikt 2.1.0](https://ajalt.github.io/clikt/)
- [For Development] [Kscript Annotations 1.4](https://github.com/holgerbrandl/kscript-annotations)
- [For Testing] [Groovy 2.5.7](http://www.groovy-lang.org/)

Use [SDKMan](http://sdkman.io/) or [Posh-GVM](http://github.com/flofreud/posh-gvm) to get Groovy, Java, Kotlin, and Kscript.


## Usage
- Run `kscript solvecnf.kts -c <cnf file>`
 
```
Usage: kscript solvecnf.kts [OPTIONS]

  Evolutionary algorithm based CNF solver

Options:
  -c, --cnf TEXT              CNF file in DIMACS format
                              (http://www.satcompetition.org/2009/format-benchmarks2009.html)
  -g, --maxGens INT           Maximum number of generations. (Default: #vars *
                              2)
  -p, --populationSize INT    Population size.  (Default: #clauses * 8)
  --mutation-threshold FLOAT  Mutation threshold.  (Default: 0.8)
  -h, --help                  Show this message and exit
```

 
## Attribution

Copyright (c) 2019 Venkatesh-Prasad Ranganath

Licensed under BSD 3-clause "New" or "Revised" License (https://choosealicense.com/licenses/bsd-3-clause/)

Authors: Venkatesh-Prasad Ranganath
