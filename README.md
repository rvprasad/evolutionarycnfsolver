# EvolutionaryCNFSolver

A tool to solve non-trivial CNF formulae (i.e., with at least one clause) using evolutionary algorithm -- pick random assignments as parents, combine, mutate, and select surviving assignments based on number of clauses satisfied by an assignment.

The tool provides information about the number of seen/expected variables and seen/expected clauses along with some info about the runtime behavior of the underlying algorithm.  The implementation of the tool in different languages is available in corresponding folder -- _Go_ and _Kotlin_.


## Attribution

Copyright (c) 2017 Venkatesh-Prasad Ranganath

Licensed under [BSD 3-clause "New" or "Revised" License](https://choosealicense.com/licenses/bsd-3-clause/)

**Authors:** Venkatesh-Prasad Ranganath
